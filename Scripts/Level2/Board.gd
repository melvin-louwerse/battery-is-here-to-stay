extends "res://Scripts/Lvl1/BaseItem.gd"

var leverPos
var leverSet
var	useOffset = 0
var	alreadyUsed = [
	   "You stick the srewdriver in the wires and get a shock",
	   "What part of getting a shock you did not understand?",
	   "bzzzz fizzle sparkle",
	   "You enjoy that don't you?"
	]
var used = false
var hasItem = false
var lev1pos = 1
var lev2pos = 1
# Called when the node enters the scene tree for the first time.
func _ready():
	lookText = "Its a panel with levers, maybe try to play with them"
	useText = "Nope i think you want the levers"
	
	talkText = "errrr... ?"

	
	pass # Replace with function body.



func finishUse() -> void:
	.finishUse()
	pass


func _on_lever_change(viewport, event, shape_idx, pos, lever):
	
	leverPos = pos
	leverSet = lever
	if (event is InputEventMouseButton and event.is_pressed() and event.button_index == BUTTON_LEFT):
		
		if (world.currentMode == world.modes.look):
			lookLever()
		elif (world.currentMode == world.modes.use):
			useLever()
		elif (world.currentMode == world.modes.walk):
			walkLever()
		elif (world.currentMode == world.modes.talk):
			talkLever()
	
	pass # Replace with function body.


func lookLever():
	var my_func = funcref(self, "finischLookSwitch")
	player.setDestination(walkPosition.get_global_transform().get_origin(), my_func)
	pass
	
func useLever():
	var my_func = funcref(self, "finischUseSwitch")
	player.setDestination(walkPosition.get_global_transform().get_origin(), my_func)
	pass
	
func walkLever():
	pass

	
func talkLever():
	var my_func = funcref(self, "finischTalkSwitch")
	player.setDestination(walkPosition.get_global_transform().get_origin(), my_func)
	pass
				

func finischUseSwitch() -> void:
	text.set_text("you switch the lever")
	print('leverpos', leverPos)
	if leverSet == 1:
		lev1pos = leverPos
		if leverPos ==  0:
			get_node("leverrightdown").set_visible(false)
			get_node("leverrightmid").set_visible(true)
		elif leverPos ==  1:
			get_node("leverrightup").set_visible(true)
			get_node("leverrightmid").set_visible(false)
		elif leverPos ==  2:
			get_node("leverrightup").set_visible(false)
			get_node("leverrightdown").set_visible(true)
	elif leverSet == 2:
		lev2pos = leverPos
		if leverPos ==  0:
			get_node("levermiddown").set_visible(false)
			get_node("levermidmid").set_visible(true)
		elif leverPos ==  1:
			get_node("levermidup").set_visible(true)
			get_node("levermidmid").set_visible(false)
		elif leverPos ==  2:
			get_node("levermidup").set_visible(false)
			get_node("levermiddown").set_visible(true)
			
	elif leverSet == 3:
		if leverPos ==  0:
			get_node("leverleftdown").set_visible(false)
			get_node("leverleftmid").set_visible(true)
		elif leverPos ==  1:
			print ('check', lev1pos, lev2pos)
			if (lev1pos == 0 and lev2pos == 0):
				get_node("leverleftup").set_visible(true)
				get_node("leverleftmid").set_visible(false)
				text.set_text("You hear water running and a loud clang")
				get_node("water").set_visible(true)
				get_node("Area2D").set_visible(true)
				textContainer.set_visible(true)
				player.get_node("room/Grate").hasScrewdriver = true
				world.addInventoryItem(3)
			else:
				text.set_text("The lever won't move it feels like a safetylock")
				textContainer.set_visible(true)
		elif leverPos ==  2:
			get_node("leverleftup").set_visible(false)
			get_node("leverleftdown").set_visible(true)
			
	textContainer.set_visible(true)
	pass

func finischTalkSwitch() -> void:
	text.set_text("?????")
	textContainer.set_visible(true)
	pass

func finischWalkSwitch() -> void:
	pass

func finischLookSwitch() -> void:
	text.set_text("Ohhhhh what does this lever do")
	textContainer.set_visible(true)
	pass

func finishUseObject() -> void:
	if (hasItem == false):
		world.addInventoryItem(4)
		get_node("panelopen").set_visible(true)
		hasItem = true
	else:
		text.set_text(alreadyUsed[useOffset])
		useOffset = min(3, useOffset + 1)
	pass