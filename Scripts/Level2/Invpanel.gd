extends TextureButton

var gameWorld
var text: Label
var textContainer
var wire = load("res://MouseIcons/pane.png")

func _ready():
	gameWorld = get_tree().get_root().get_node('World')
	textContainer = gameWorld.find_node('Control')
	text = textContainer.find_node('Label')
	pass

func _on_TextureButton_pressed():
	
	if (gameWorld.isLooking()):
		text.set_text("A metal panel")
		textContainer.set_visible(true)
	elif (gameWorld.isUsing()):
		Input.set_custom_mouse_cursor(wire, Input.CURSOR_ARROW, Vector2(10	,10))
		gameWorld.currentMode = gameWorld.modes.use_object
		gameWorld.currentUseItem = gameWorld.useItems.panel
		gameWorld.removeItemFromInv(3)
	pass
