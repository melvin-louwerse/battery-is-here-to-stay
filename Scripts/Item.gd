extends Sprite

var world
var text: Label
var textContainer

# Called when the node enters the scene tree for the first time.
func _ready():
	world = get_tree().get_root().get_node('World')
	textContainer = world.find_node('Control')
	text = textContainer.find_node('Label')
	pass # Replace with function body.

func _on_Area2D_mouse_entered():
	world.setOverObject(true)
	pass # Replace with function body.


func _on_Area2D_mouse_exited():
	world.setOverObject(false)
	pass # Replace with function body.

func _on_Area2D_input_event(viewport, event, shape_idx):
	if (event is InputEventMouseButton and event.is_pressed() and event.button_index == BUTTON_LEFT):
		if (world.currentMode == world.modes.look):
			look()
		elif (world.currentMode == world.modes.use):
			use()
		elif (world.currentMode == world.modes.walk):
			walk()
		elif (world.currentMode == world.modes.talk):
			talk()
	pass

func finishLook() -> void:
	text.set_text("Oh boy, It's a generator exactly what you need. Now if it was only working")
	textContainer.set_visible(true)
	print("finish")
	pass
	
func finishUse() -> void:
	text.set_text("Stop fondeling that geneator it is not workling you pervert")
	textContainer.set_visible(true)
	pass
	
func finishTalk() -> void:
	text.set_text("You try to have a intesting conversation with the generator, you feel foolish")
	textContainer.set_visible(true)
	pass

func look() -> void:
	var my_func = funcref(self, "finishLook")
	get_parent().get_parent().setDestination(get_node('Position2D').get_global_transform().get_origin(), my_func)
	pass

func walk() -> void:
	get_parent().get_parent().setDestination(get_node('Position2D').get_global_transform().get_origin(), null)
	pass
	
func talk() -> void:
	var my_func = funcref(self, "finishTalk")
	get_parent().get_parent().setDestination(get_node('Position2D').get_global_transform().get_origin(), my_func)
	pass
		
func use():
	var my_func = funcref(self, "finishUse")
	get_parent().get_parent().setDestination(get_node('Position2D').get_global_transform().get_origin(), my_func)
	pass	
