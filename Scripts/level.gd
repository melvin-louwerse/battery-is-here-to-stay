extends Node2D

var bot
var botAnim
var world
var camera: Camera2D

var speed = 3
var destination = null
var currentCallback = null

var code1 = 1;#red
var code2 = 3;#blue
var code3 = 1;#red
var code4 = 4;#yellow
var code5 = 2;#orange

var gCode1 = 0;
var gCode2 = 0;
var gCode3 = 0;
var gCode4 = 0;
var gCode5 = 0;

func setColor(pos, color):
	
	var col = Color(1, 0, 0, 1)
	
	if (color == 1):
		col = Color(1, 0, 0, 1)
	elif (color == 2):
		col = Color(1, 0.55, 0, 1)
	elif (color == 3):
		col = Color( 0, 0, 1, 1)
	else:
		col = Color(1, 1, 0, 1)
		
	var choice = get_node("room/choice" + str(pos))
	choice.color = col
	pass
	
func setGues(i):
	if gCode1 == 0:
		gCode1 = i
		setColor(1,i)
	elif gCode2 == 0:
		gCode2 = i
		setColor(2,i)
	elif gCode3 == 0:
		gCode3 = i
		setColor(3,i)
	elif gCode4 == 0:
		gCode4 = i
		setColor(4,i)
	elif gCode5 == 0:
		gCode5 = i
		setColor(5,i)
		showGuess()
	else:
		gCode1 = 0
		gCode2 = 0
		gCode3 = 0
		gCode4 = 0
		gCode5 = 0
		clearGuess()
		clearResult()
		setGues(i)
		
	pass


func clearGuess():
	for i in range(1, 6):
		var choice = get_node("room/choice" + str(i))
		choice.color = Color( 1, 1, 1, 1)
	
func clearResult():
	for i in range(1, 6):
		var choice = get_node("room/try" + str(i))
		choice.color = Color( 1, 1, 1, 1)
		
func showGuess():
	var correct = 0;
	
	if code1 == gCode1:
		correct+=1;
	if code2 == gCode2:
		correct+=1;
	if code3 == gCode3:
		correct+=1;
	if code4 == gCode4:
		correct+=1;
	if code5 == gCode5:
		correct+=1		
		
	var good = Color(0, 1, 0, 1)
	var wrong = Color(1, 0, 0, 1)
	
	
	if (correct == 5):
		var textContainer = world.find_node('Control')
		var text = textContainer.find_node('Label')
		text.set_text("You broke the code, i guess you can charge and live an other day")
		world.get_node("CanvasLayer/fin").set_visible(true)
		textContainer.set_visible(true)
	for i in range(1, 6):
		var choice = get_node("room/try" + str(i))
		print (i)
		if (correct >= i):
			choice.color = good
		else:
			choice.color = wrong
			
	pass
# Called when the node enters the scene tree for the first time.
func _ready():
	print("lvl started")
	world = get_tree().get_root().get_node('World')
	bot = find_node("Bot");
	camera = world.get_node("MainCam");
	botAnim = bot.get_node('AnimationPlayer')
	camera.set_global_position(bot.get_global_position() + Vector2(0, -200))
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if destination != null:
		var velocity = Vector2(0 , 0)
		
		if (bot.get_global_position().x < destination.x + speed && bot.get_global_position().x > destination.x - speed):
			velocity.x  = 0
		elif (bot.get_global_position().x < destination.x):
			velocity.x  = 1
			bot.get_node("Container").set_scale(Vector2(1, 1))
			bot.get_node("Skeleton2D").set_scale(Vector2(1, 1))
		elif (bot.get_global_position().x > destination.x - speed): 
			velocity.x  = -1
			bot.get_node("Container").set_scale(Vector2(-1, 1))
			bot.get_node("Skeleton2D").set_scale(Vector2(-1, 1))	
		if (bot.get_global_position().y < destination.y + speed && bot.get_global_position().y > destination.y - speed):
			velocity.y  = 0	
		elif (bot.get_global_position().y < destination.y - speed):
			velocity.y  = 1
		elif (bot.get_global_position().y > destination.y - speed):
			velocity.y  = -1
			
		if (velocity.x == 0 && velocity.y == 0):
			destination = null
			botAnim.play("Rest", 0)
			
			if (currentCallback != null):
				currentCallback.call_func()
			
		else:
			botAnim.play("Walk", 0)
			
			
		bot.move_and_collide(velocity * speed)
	camera.set_global_position(bot.get_global_position() + Vector2(0, -200))
	pass

func setDestination(dest, callback = null):
	destination = dest
	currentCallback = callback
	pass
	
func _OnClick(viewport, event, shape_idx):
	if event is InputEventMouseButton and world.overObject == false and world.currentMode == world.modes.walk:
		setDestination(get_global_mouse_position())
	pass # Replace with function body.
