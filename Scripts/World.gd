extends Node2D

class_name GameWorld

export var startLevel = 1

const items = [
	preload("res://Inventory/KeyInv.tscn"),
	preload("res://Inventory/WireInv.tscn"),
	preload("res://Inventory/InstructionsInv.tscn"),
	preload("res://Inventory/ScrewInv.tscn"),
	preload("res://Inventory/PanelInv.tscn")
];

var itemDictionary = {
	0: {
		"itemName": "Key",
		"item": items[0]
	},
	1: {
		"itemName": "Wire",
		"item": items[1]
	},
	2: {
		"itemName": "Instructions",
		"item": items[2]
	},
	3: {
		"itemName": "Screwdriver",
		"item": items[3]
	},
	4: {
		"itemName": "panel",
		"item": items[4]
	}
};

var overObject = false;

var eyeOpen = load("res://MouseIcons/eyeopen.png")
var eyeClose = load("res://MouseIcons/eyeclose.png")

var talk = load("res://MouseIcons/talk.png")
var noTalk = load("res://MouseIcons/notalk.png")

var walk = load("res://MouseIcons/walk.png")
var noWalk = load("res://MouseIcons/nowalk.png")

var use = load("res://MouseIcons/use.png")
var noUse = load("res://MouseIcons/nouse.png")

var inventory_items = []

var inv;
enum modes{
	walk,
	talk,
	look,
	use,
	use_object
}

enum useItems{
	copper,
	key,
	instruction,
	screw,
	panel
}

var currentUseItem = null;

var currentItem = null;

var currentMode = modes.walk
# Called when the node enters the scene tree for the first time.
func _ready():
	inv = get_node("CanvasLayer").get_node("Control2").get_node("inventory")
	pass # Replace with function body.

func changeMode(mode):
	currentMode = mode
	changeCursor()
	pass
	
func setOverObject(over):
	overObject = over
	if over == true:
		var icon
		var pos = Vector2(16,16)
		match currentMode:
				modes.walk:
					icon = walk
					#pos = Vector2(5, 55)
					Input.set_custom_mouse_cursor(icon, Input.CURSOR_ARROW, pos)
				modes.talk:
					icon = talk
					#pos = Vector2(9, 55)
					Input.set_custom_mouse_cursor(icon, Input.CURSOR_ARROW, pos)
				modes.look:
					icon = eyeOpen
					#pos = Vector2(32, 32)
					Input.set_custom_mouse_cursor(icon, Input.CURSOR_ARROW, pos)
				modes.use:
					icon = use
					#pos = Vector2(49, 5)
					Input.set_custom_mouse_cursor(icon, Input.CURSOR_ARROW, pos)
	else:
		changeCursor()
				
	pass


func setCursorToInv():
	print("hier")
	
	
func isLooking():
	return currentMode == modes.look
	
	
func isUsing():
	return currentMode == modes.use

func changeCursor():
	var icon
	var pos = Vector2(16,16)
	match currentMode:
			modes.walk:
				icon = walk
				Input.set_custom_mouse_cursor(icon, Input.CURSOR_ARROW, pos)
			modes.talk:
				icon = noTalk
				if inv.is_visible():
					icon = talk
				else:
					icon = noTalk
				Input.set_custom_mouse_cursor(icon, Input.CURSOR_ARROW, pos)
			modes.look:
				if inv.is_visible():
					icon = eyeOpen
				else:
					icon = eyeClose
					
				Input.set_custom_mouse_cursor(icon, Input.CURSOR_ARROW, pos)
			modes.use:
				if inv.is_visible():
					icon = use
				else:
					icon = noUse
				Input.set_custom_mouse_cursor(icon, Input.CURSOR_ARROW, pos)
	pass

func changeCursorInv():
	var icon
	var pos = Vector2(16,16)
	match currentMode:
		modes.walk:
			icon = walk
			Input.set_custom_mouse_cursor(icon, Input.CURSOR_ARROW, pos)
		modes.talk:
			icon = talk
			Input.set_custom_mouse_cursor(icon, Input.CURSOR_ARROW, pos)
		modes.look:
			icon = eyeOpen
			Input.set_custom_mouse_cursor(icon, Input.CURSOR_ARROW, pos)
		modes.use:
			icon = use
			Input.set_custom_mouse_cursor(icon, Input.CURSOR_ARROW, pos)
	pass
		
func _input(event):
	# Mouse in viewport coordinates
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_L:
			changeMode(modes.look)
		if event.scancode == KEY_W:
			changeMode(modes.walk)
		if event.scancode == KEY_U:
			changeMode(modes.use)
		if event.scancode == KEY_T:
			changeMode(modes.talk)
			
		

func hideInv():
	inv.set_visible(false)
	
func _on_Inv_pressed():
	inv.set_visible(!inv.is_visible())
	
	if (inv.is_visible()):
		currentMode = modes.look
		changeCursor()
	pass # Replace with function body

func clearInventorySlots():
	for i in range(1, 5):
		var slot = inv.get_node("slot" +  str(i));
		
		for c in slot.get_children():
			if c is Position2D:
				print(c)
				#do nothing
			else:
				slot.remove_child(c)
				c.free()

func removeItemFromInv(item):
	inventory_items.erase(itemDictionary[item])
	
	clearInventorySlots()
	
	for i in range(0, inventory_items.size()):
		var slot = inv.get_node("slot" +  str(i + 1));
		var GrabedInstance= inventory_items[i].item.instance()
		GrabedInstance.set_position(slot.get_node("Position2D").get_position())
		slot.add_child(GrabedInstance)
		
	
func addInventoryItem(item):
	print(item)
	print(itemDictionary[item])
	inventory_items.append(itemDictionary[item])
	
	for i in range(0, inventory_items.size()):
		var slot = inv.get_node("slot" +  str(i + 1));
		var GrabedInstance= inventory_items[i].item.instance()
		GrabedInstance.set_position(slot.get_node("Position2D").get_position())
		slot.add_child(GrabedInstance)

func _on_look_pressed():
	currentMode = modes.look
	changeCursor()
	pass # Replace with function body.


func _on_walk_pressed():
	currentMode = modes.walk
	changeCursor()
	pass # Replace with function body.


func _on_use_pressed():
	currentMode = modes.use
	changeCursor()
	pass # Replace with function body.

func _on_talk_pressed():
	currentMode = modes.talk
	changeCursor()
	pass # Replace with function body.
	
func setLevel(lvl):
	
	if (lvl == 1):
		var lvl1 = load("res://Scenes/lvl1.tscn")
	
		var instance = lvl1.instance()
		add_child(instance)
		get_node("MainCam").set_global_position(instance.get_node("room/Bot").get_global_position() + Vector2(0, -100))	
		
	if (lvl == 2):
		var lvl2 = load("res://Scenes/lvl2.tscn")
	
		var instance = lvl2.instance()
		remove_child(get_node("lvl1"))
		add_child(instance)
		get_node("MainCam").set_global_position(instance.get_node("room/Bot").get_global_position() + Vector2(0, -100))
		
	if (lvl == 3):
		var lvl3 = load("res://Scenes/lvl3.tscn")
	
		var instance = lvl3.instance()
		remove_child(get_node("lvl2"))
		add_child(instance)
		get_node("MainCam").set_global_position(instance.get_node("room/Bot").get_global_position() + Vector2(0, -100))	
	

func _start():
	get_node("CanvasLayer/Control2").set_visible(true)
	get_node("CanvasLayer/Button").set_visible(false)
	get_node("Start").set_visible(false)
	setLevel(startLevel)
	pass # Replace with function body.
