extends Node2D

var world
var text: Label
var textContainer

var lookText = "Base Look Text"
var useText = "Base Use Text"# Called when the node enters the scene tree for the first time.
var talkText = "Base Talk Text"

var player
var walkPosition

#var	useOffset = 0
#var	alreadyUsed = [
#	   "Already used 1",
#	   "Already used 1",
#	   "Already used 3",
#	   "Already used 4"
#	]
#var used = false

func _ready():
	world = get_tree().get_root().get_node('World')
	textContainer = world.find_node('Control')
	text = textContainer.find_node('Label')
	player = get_parent().get_parent()
	walkPosition = get_node('Position2D')
	pass

func _on_Area2D_mouse_entered():
	world.setOverObject(true)
	pass

func _on_Area2D_mouse_exited():
	world.setOverObject(false)
	pass

func _on_Area2D_input_event(viewport, event, shape_idx):
	if (event is InputEventMouseButton and event.is_pressed() and event.button_index == BUTTON_LEFT):
		if (world.currentMode == world.modes.look):
			look()
		elif (world.currentMode == world.modes.use):
			use()
		elif (world.currentMode == world.modes.walk):
			walk()
		elif (world.currentMode == world.modes.talk):
			talk()
		elif (world.currentMode == world.modes.use_object):
			useObject()
	pass

func finishLook() -> void:
	text.set_text(lookText)
	textContainer.set_visible(true)
	pass
	
func finishUse() -> void:
	text.set_text(useText)
	textContainer.set_visible(true)
	pass
	
func finishTalk() -> void:
	text.set_text(talkText)
	textContainer.set_visible(true)
	pass
	
func finishUseObject() -> void:
	
	pass

func useObject() -> void:
	var my_func = funcref(self, "finishUseObject")
	player.setDestination(walkPosition.get_global_transform().get_origin(), my_func)
	pass
	
func look() -> void:
	var my_func = funcref(self, "finishLook")
	player.setDestination(walkPosition.get_global_transform().get_origin(), my_func)
	pass

func walk() -> void:
	player.setDestination(walkPosition.get_global_transform().get_origin(), null)
	pass
	
func talk() -> void:
	var my_func = funcref(self, "finishTalk")
	player.setDestination(walkPosition.get_global_transform().get_origin(), my_func)
	pass
		
func use():
	var my_func = funcref(self, "finishUse")
	player.setDestination(walkPosition.get_global_transform().get_origin(), my_func)
	pass
