# level1
* Use the poster to get the manual
* Use manual in inventory to read it
* Use the doormat
* Use the copper wire in your inventory and then use it on yourself
* Reach into the hole to get the key
* Use key from inventory on door

# level2
* Use control board  move first lever once  (to the middle) move second lever 3 times( to the middle again_ move third lever to the top
- reach into the grate to get the screw driver
- use screwdriver from inventory on the control board
- use control board on the mouse hole
- mouse kicks down voice box, use it to pick it up
- use voicebox on yourself
- talk to the voiceconsole near the door

# level3
Just a puzzle not going to give away the sequence but might be good to know that the second row of boxes
show green if you had a color on the correct position and red for not (it does not show you the position just how many you had correct)

