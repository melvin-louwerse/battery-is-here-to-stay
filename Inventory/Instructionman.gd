extends TextureButton

var gameWorld
var text: Label
var textContainer
var wire = load("res://MouseIcons/booklet.png")

func _ready():
	gameWorld = get_tree().get_root().get_node('World')
	textContainer = gameWorld.find_node('Control')
	text = textContainer.find_node('Label')
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_TextureButton_pressed():
	
	if (gameWorld.isLooking()):
		text.set_text("This looks like a instruction on magnetism")
		textContainer.set_visible(true)
	elif (gameWorld.isUsing()):
		text.set_text("You read the manual, this will come in handy")
		textContainer.set_visible(true)
		gameWorld.get_node('lvl1/room/Bot').hasInstruction = true
		gameWorld.removeItemFromInv(2)
		
	pass # Replace with function body.
