extends TextureButton

var gameWorld
var text: Label
var textContainer
var wire = load("res://MouseIcons/copper.png")

func _ready():
	gameWorld = get_tree().get_root().get_node('World')
	textContainer = gameWorld.find_node('Control')
	text = textContainer.find_node('Label')
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_TextureButton_pressed():
	
	if (gameWorld.isLooking()):
		text.set_text("This a length of highly conductive copper wire")
		textContainer.set_visible(true)
	elif (gameWorld.isUsing()):
		Input.set_custom_mouse_cursor(wire, Input.CURSOR_ARROW, Vector2(78,0))
		gameWorld.currentMode = gameWorld.modes.use_object
		gameWorld.currentUseItem = gameWorld.useItems.copper
		
	pass # Replace with function body.
