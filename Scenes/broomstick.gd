extends "res://Scripts/Lvl1/BaseItem.gd"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var broomOpen = false

# Called when the node enters the scene tree for the first time.
func _ready():
	lookText = "Feeling like cleaning the floor?"
	useText = "You took the broom"
	talkText = "errrr... ?"
	._ready()
	player = get_parent().get_parent().get_parent()
	
	pass # Replace with function body.

func finishUse() -> void:
	text.set_text(useText)
	textContainer.set_visible(true)
	world.addInventoryItem(0)
	pass