extends "res://Scripts/Lvl1/BaseItem.gd"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	lookText = "You see someting shiny inside"
	useText = "You reach inside but seem not to be able to reach the shiny thing"
	talkText = "errrr... ?"

	pass # Replace with function body.

func finishUse() -> void:
	.finishUse()
	if (player.get_node('room/Bot').hasMagnet):
		text.set_text("You reach inside and feel a clank as your arm atracts the object. You found a key")
		world.addInventoryItem(0)
		textContainer.set_visible(true)
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
