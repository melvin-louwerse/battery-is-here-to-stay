extends "res://Scripts/Lvl1/BaseItem.gd"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var hasItem = false
# Called when the node enters the scene tree for the first time.
func _ready():
	lookText = "Better not let my battery run out then ..bzzzt..."
	useText = "You look behind the poster an find an instruction manual"
	talkText = "errrr... ?"

	pass # Replace with function body.


func finishUse() -> void:
	.finishUse()
	if (hasItem == false):
		world.addInventoryItem(2)
		useText = "You see a faint imprint of an instruction manual ... what a suprise"
		hasItem = true
	
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
