extends KinematicBody2D

var world
var text: Label
var textContainer


var hasMagnet = false
var hasInstruction = false
var hasVoice = false

func _ready():
	world = get_tree().get_root().get_node('World')
	textContainer = world.find_node('Control')
	text = textContainer.find_node('Label')
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Boty_input_event(viewport, event, shape_idx):
	pass # Replace with function body.


func _on_Boty_mouse_entered():
	pass # Replace with function body.


func _on_Area2D_input_event(viewport, event, shape_idx):
	if (event is InputEventMouseButton and event.is_pressed() and event.button_index == BUTTON_LEFT):
		if (world.currentUseItem == world.useItems.copper and hasInstruction):
			text.set_text("You wrap the copper around your arm. You feel like you have a magnetic personality")
			textContainer.set_visible(true)
			get_node("Container/ArmLowerRightCopper").set_visible(true)
			world.removeItemFromInv(1)
			hasMagnet = true
		elif (world.currentUseItem == world.useItems.copper and !hasInstruction):
			text.set_text("You feel like you are on the right way, if only you only had some instructions")
			textContainer.set_visible(true)
	pass # Replace with function body.


func foot_step():
	get_node("AudioStreamPlayer").play()