extends "res://Scripts/Lvl1/BaseItem.gd"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var locked = true
# Called when the node enters the scene tree for the first time.
func _ready():
	lookText = "It is a door, what more you want me to tell?"
	useText = "It is locked"
	talkText = "errrr... ?"

	pass # Replace with function body.


func finishLook() -> void:
	if (locked):
		text.set_text(lookText)
	else:
		text.set_text("When is a door not a door? when its ajar. But this one is unlocked")
		
	textContainer.set_visible(true)
	pass
	
	
func finishUse() -> void:
	
	if (locked):
		text.set_text(useText)
		textContainer.set_visible(true)
	else:
		text.set_text("Well done level is done!!!")
		textContainer.set_visible(true)
		world.setLevel(2)
		
	pass
	
func finishUseObject() -> void:
	.finishUseObject()
	if (world.currentUseItem == world.useItems.key):
		text.set_text("You Unlocked the door")
		world.removeItemFromInv(0)
		textContainer.set_visible(true)
		world.currentUseItem = null
		world._on_look_pressed()
		world.hideInv()
		locked = false
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
