extends "res://Scripts/Lvl1/BaseItem.gd"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var broomOpen = false

# Called when the node enters the scene tree for the first time.
func _ready():
	lookText = "Its a supplies closet, looks like it can open"
	useText = "You open the closet"
	talkText = "Talking to a closet how low have you sunk?"
	._ready()
	pass # Replace with function body.

func finishUse() -> void:
	text.set_text(useText)
	textContainer.set_visible(true)
	texture = load("res://character/broomopen.png")
	broomOpen = true
	get_node("Area2D").set_visible(false)
	get_node("broomstick").set_visible(true)
	pass